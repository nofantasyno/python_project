import math
import telebot
import requests
import json
import subprocess
import time
import urllib.parse
import csv
import json
import subprocess

token = "886035760:AAFL0VHZU-9B4MZE_vhvnDAZI7sU2G9pH-4"
bot = telebot.TeleBot(token)
userId = ""


@bot.message_handler(content_types=['text'])
def get_text_messages(message):
    if message.text:
        r = requests.get("https://www.instagram.com/{}".format(message.text))
        array = r.text.split('"')
        profilePage = ''.join([i for i in array if i.startswith('profilePage_')])

        if profilePage != '':
            userId = profilePage.split("_")[1]
            print(userId)
            get_followers(userId, message.from_user.id)
        else:
            bot.send_message(message.from_user.id, "Wrong name")


def get_followers(data, chat_id):
    userId = data
    url_base = 'https://www.instagram.com/graphql/query/?'

    command_template = """curl '{url}' -H 'pragma: no-cache' -H 'cookie: mid=W75TDAALAAHjgif2RuU2BxrZjCfI; mcd=3; csrftoken=II7NqnFZNxgTSRpOTcTLwBJKIgJTcgRA; ds_user_id='{userId}'; ig_cb=1; sessionid=3924897297%3A9a2DmQufoFCtmw%3A2; fbm_124024574287414=base_domain=.instagram.com; shbid=15760; shbts=1553692927.3305106; rur=FRC; urlgen="{{\"212.76.14.177\": 21299}}:1hADW6:csxxEKDTM2ayk1GcE8pEcKY01Sw"' -H 'x-ig-app-id: 936619743392459' -H 'accept-encoding: gzip, deflate, br' -H 'accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,bg;q=0.6' -H 'user-agent: Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Mobile Safari/537.36' -H 'accept: */*' -H 'cache-control: no-cache' -H 'authority: www.instagram.com' -H 'x-requested-with: XMLHttpRequest' -H 'x-instagram-gis: 2202b086b12095636b00d681f14c6bb8' -H 'referer: https://www.instagram.com/'username'/followers/' --compressed > json/followers_{index}.json"""

    index = 1
    after = None
    followers_in_progress = 0
    while True:
        after_value = f',"after":"{after}"' if after else ''
        variables = f'{{"id":"{userId}","include_reel":true,"fetch_mutual":false,"first":50{after_value}}}'
        get_params = {
            'query_hash': '56066f031e6239f35a904ac20c9f37d9',
            'variables': variables
        }
        ws_url = url_base + urllib.parse.urlencode(get_params)
        print(ws_url)
        result = subprocess.run(command_template.format(url=ws_url, index=index, userId=userId), shell=True)

        print(result.__dict__)

        if result.returncode != 0:
            exit('Кто то украл данные, все плохо')

        with open(f'json/followers_{index}.json', 'r') as f:
            data = json.load(f)

        if not data['data']['user']['edge_followed_by']['page_info']['has_next_page']:
            break

        after = data['data']['user']['edge_followed_by']['page_info']['end_cursor']
        all_followers = data['data']['user']['edge_followed_by']['count']
        in_current_batch = len(data['data']['user']['edge_followed_by']['edges'])
        followers_in_progress += in_current_batch
        print(f'Обработано {followers_in_progress}/{all_followers}')

        time.sleep(5 if index % 10 != 0 else 20)
        index += 1

    print('#деловшляпе')
    exec(open("parse_followers.py").read())
    time.sleep(3)

    with open('followers.json', 'r') as f:
        followers = json.load(f)

    command_template = """curl 'https://www.instagram.com/{username}/?__a=1' -H 'pragma: no-cache' -H 'cookie: mid=W75TDAALAAHjgif2RuU2BxrZjCfI; mcd=3; csrftoken=II7NqnFZNxgTSRpOTcTLwBJKIgJTcgRA; ds_user_id=3924897297; ig_cb=1; sessionid=3924897297%3A9a2DmQufoFCtmw%3A2; fbm_124024574287414=base_domain=.instagram.com; shbid=15760; shbts=1558927608.7240922; rur=FRC; urlgen="{{\"212.76.6.86\": 13082}}:1hV6VG:qFmIFUi9MubaFAA9wfVbPs6blT8"' -H 'x-ig-app-id: 1217981644879628' -H 'accept-encoding: gzip, deflate, br' -H 'accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,bg;q=0.6' -H 'user-agent: Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Mobile Safari/537.36' -H 'accept: */*' -H 'cache-control: no-cache' -H 'authority: www.instagram.com' -H 'x-requested-with: XMLHttpRequest' -H 'referer: https://www.instagram.com/anelkanagatova/' --compressed > temp.json"""

    index = 0
    followers_filled = []
    for user in followers:
        subprocess.run(command_template.format(username=user['username'], userId=userId), shell=True)
        with open('temp.json', 'r') as f:
            data = json.load(f)
            print(data)

        if 'graphql' not in data:
            time.sleep(5)
            print(f'нет graphql в ответе сервера, ответ: {data}')
            continue

        photos_edges = data['graphql']['user']['edge_owner_to_timeline_media']
        last_photos_posted_in_one_day = False
        if photos_edges and len(photos_edges['edges']) > 2:
            last_photos_posted_at = [photos_edges['edges'][i]['node']['taken_at_timestamp'] for i in range(3)]
            difference = int(math.fabs(min(last_photos_posted_at) - max(last_photos_posted_at)))
            if difference < 86400:
                last_photos_posted_in_one_day = True

        user['follows'] = data['graphql']['user']['edge_follow']['count']
        user['posts'] = data['graphql']['user']['edge_owner_to_timeline_media']['count']
        user['biography'] = data['graphql']['user']['biography']
        user['last_photos_posted_in_one_day'] = last_photos_posted_in_one_day
        #new data
        user['avg_like'] = 0
        user['avg_comment'] = 0
        for post in data['graphql']['user']['edge_owner_to_timeline_media']['edges']:
            user['avg_comment'] += post['node']['edge_media_to_comment']['count']
            user['avg_like'] += post['node']['edge_liked_by']['count']
        user['avg_like'] = user['avg_like'] / user['posts']
        user['avg_comment'] = user['avg_comment'] / user['posts']

        followers_filled.append(user)

        print(f'Итерация {index}/{len(followers)}')
        time.sleep(0.5 if index % 10 != 0 else 3)
        if index % 10 == 0:
            with open(f'filled/followers_filled_{index}.json', 'w+') as f:
                json.dump(followers_filled, f)
        index += 1

    with open('followers_filled.csv', 'w+', newline='') as f:
        writer = csv.writer(f, delimiter=';')
        writer.writerow(['Имя', 'Username', 'Подписан на', 'Постов', 'Среднее кол-во лайков', 'Среднее кол-во постов', 'Био', 'Последние фото в 1 день'])
        print(followers_filled)
        for user in followers_filled:
            full_name = user["full_name"].replace('"', "")
            bio = user["biography"].replace('"', "")
            writer.writerow([
                full_name,
                user["username"],
                user["follows"],
                user["posts"],
                user["avg_like"],
                user["avg_comment"],
                bio,
                1 if user["last_photos_posted_in_one_day"] else 0
            ])

    print('#готоводело')

    bot.send_document(chat_id, open("followers_filled.csv", 'rb'))


bot.polling(none_stop=True, interval=0)
