import csv
import json
import subprocess

with open('followers.json', 'r') as f:
    followers = json.load(f)

command_template = """curl 'https://www.instagram.com/{username}/?__a=1' -H 'pragma: no-cache' -H 'cookie: mid=W75TDAALAAHjgif2RuU2BxrZjCfI; mcd=3; csrftoken=II7NqnFZNxgTSRpOTcTLwBJKIgJTcgRA; ds_user_id={userId}; ig_cb=1; sessionid=3924897297%3A9a2DmQufoFCtmw%3A2; fbm_124024574287414=base_domain=.instagram.com; shbid=15760; shbts=1553952392.2688835; rur=FRC; urlgen="{{\"46.34.205.164\": 21299}}:1hAb1J:zsp4L5DQcaQEknS9jwzKRjL-rDo"' -H 'x-ig-app-id: 936619743392459' -H 'accept-encoding: gzip, deflate, br' -H 'accept-language: ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,bg;q=0.6' -H 'user-agent: Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/73.0.3683.86 Mobile Safari/537.36' -H 'accept: */*' -H 'cache-control: no-cache' -H 'authority: www.instagram.com' -H 'x-requested-with: XMLHttpRequest' -H 'x-instagram-gis: b71e5550c402c618f5442175c20538fe' -H 'referer: https://www.instagram.com/{username}' --compressed > temp.json"""

index = 0
followers_filled = []
for user in followers:
    subprocess.run(command_template.format(username=user['username']), shell=True)
    with open('temp.json', 'r') as f:
        try:
            data = json.load(f)
            print(data)
            break
        except:
            print(f'проблема с парсингом JSON в {user["username"]}')
            continue

    if 'graphql' not in data:
        time.sleep(5)
        print(f'нет graphql в ответе сервера, ответ: {data}')
        continue

    photos_edges = data['graphql']['user']['edge_owner_to_timeline_media']
    last_photos_posted_in_one_day = False
    if photos_edges and len(photos_edges['edges']) > 2:
        last_photos_posted_at = [photos_edges['edges'][i]['node']['taken_at_timestamp'] for i in range(3)]
        difference = int(math.fabs(min(last_photos_posted_at) - max(last_photos_posted_at)))
        if difference < 86400:
            last_photos_posted_in_one_day = True


    user['follows'] = data['graphql']['user']['edge_follow']['count']
    user['posts'] = data['graphql']['user']['edge_owner_to_timeline_media']['count']
    user['biography'] = data['graphql']['user']['biography']
    user['last_photos_posted_in_one_day'] = last_photos_posted_in_one_day
    followers_filled.append(user)

    print(f'Итерация {index}/{len(followers)}')
    time.sleep(0.5 if index % 10 != 0 else 3)
    if index % 10 == 0:
        with open(f'filled/followers_filled_{index}.json', 'w+') as f:
            json.dump(followers_filled, f)
    index += 1

with open('followers_filled.csv', 'w+', newline='') as f:
    writer = csv.writer(f, delimiter=';')
    writer.writerow(['Имя', 'Username', 'Подписан на', 'Постов', 'Био', 'Последние фото в 1 день'])
    print(followers_filled)
    for user in followers_filled:
        full_name = user["full_name"].replace('"', "")
        bio = user["biography"].replace('"', "")
        writer.writerow([
            full_name,
            user["username"],
            user["follows"],
            user["posts"],
            bio,
            1 if user["last_photos_posted_in_one_day"] else 0
        ])

print('#готоводело')
